package com.project.visaruth.hangman;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListEditVocab extends AppCompatActivity {
    SQLiteDatabase mDb;
    MyDbHelper mHelper;
    Cursor mCursor;
    ArrayList<String> volcab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_edit_vocab);

        mHelper = new MyDbHelper(this);
        mDb = mHelper.getWritableDatabase();
        mCursor = mDb.rawQuery("SELECT * FROM " + MyDbHelper.TABLE_NAME, null);

        volcab = new ArrayList<String>();
        mCursor.moveToFirst();

        ListView listView1 = (ListView) findViewById(R.id.listView1);

        while ( !mCursor.isAfterLast() ){
            volcab.add(mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_VOLCAB)));
            mCursor.moveToNext();
        }

        ItelListVolcab item = new ItelListVolcab();
        listView1.setAdapter(item);
    }

    public void onPause() {
        super.onPause();
        mHelper.close();
        mDb.close();
    }

    public void addVolcab(View view){
        Intent intent = new Intent(ListEditVocab.this,AddVolcabActivity.class);
        startActivity(intent);
    }

    class ItelListVolcab extends BaseAdapter{

        @Override
        public int getCount() {
            return volcab.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View cView, ViewGroup viewGroup) {
            @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.list_vocab,null);

            TextView t1 = (TextView)view.findViewById(R.id.textView2);
            t1.setText(volcab.get(i));

            ImageButton imgBtnEdit = (ImageButton)view.findViewById(R.id.imageButton);
            ImageButton imgBtnDelete = (ImageButton)view.findViewById(R.id.imageButton2);
            imgBtnDelete.setTag(volcab.get(i));
            imgBtnEdit.setTag(volcab.get(i));

            imgBtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setTitle("ลบคำศัพท์");
                    dialog.setIcon(android.R.drawable.ic_media_pause);
                    dialog.setMessage("คุณต้องการลบคำศัพท์ "+view.getTag()+" ?");
                    dialog.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();

                            mDb.execSQL("DELETE FROM " + MyDbHelper.TABLE_NAME +" WHERE "
                                    + MyDbHelper.COL_VOLCAB + " = '" + view.getTag() + "';");
                        }
                    });
                    dialog.setNegativeButton("ไม่", null);
                    dialog.show();
                }
            });

//            imgBtnEdit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(final View view) {
//                    Intent intent = new Intent(ListEditVocab.this,EditVolcabActivity.class);
//                    intent.putExtra("volcab", view.getTag()+"");
//                    startActivity(intent);
//                }
//            });

            return view;
        }
    }
}
