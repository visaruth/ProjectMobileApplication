package com.project.visaruth.hangman;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class EditVolcabActivity extends AppCompatActivity {
    SQLiteDatabase mDb;
    MyDbHelper mHelper;
    Cursor mCursor;
    private int id;
    String volcab;
    String meaning;
    String mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_volcab);

        EditText tV = (EditText)findViewById(R.id.editText);
        EditText tM = (EditText)findViewById(R.id.editText2);
        Spinner sp = (Spinner)findViewById(R.id.spinner);

        volcab = getIntent().getStringExtra("volcab");

        mHelper = new MyDbHelper(this);
        mDb = mHelper.getWritableDatabase();
        mCursor = mDb.rawQuery("SELECT * FROM " + MyDbHelper.TABLE_NAME + " WHERE " + MyDbHelper.COL_VOLCAB +" = '"+volcab+"'", null);
        mCursor.moveToFirst();

        id = mCursor.getInt(mCursor.getColumnIndex("_id"));

        tV.setText(volcab);

        meaning = mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_MEANING));
        mode = mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_MODE));

        tM.setText(mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_MEANING)));
        String compareValue = mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_MODE));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.allMode, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);

//        if (!compareValue.equals(null)) {
//            int spinnerPosition = adapter.getPosition(compareValue);
//            sp.setSelection(spinnerPosition);
//        }
    }

    public void editVolcab(View view){
        //TODO fix edit volcab in the future
//        volcab = ((EditText)findViewById(R.id.editText)).getText().toString();
//        meaning = ((EditText)findViewById(R.id.editText2)).getText().toString();
//        mode = volcab = ((Spinner)findViewById(R.id.spinner)).getSelectedItem().toString();
//        mDb.execSQL("UPDATE " + MyDbHelper.TABLE_NAME +" SET "+MyDbHelper.COL_VOLCAB+" = '"+volcab+"',"+
//                MyDbHelper.COL_MEANING+" = '"+meaning+"',"+MyDbHelper.COL_MODE+" = '"+mode+"' "+" WHERE "
//                + "_id = '" + id + "';");
//        finish();
    }

}
