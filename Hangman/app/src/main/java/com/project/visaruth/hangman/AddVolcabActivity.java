package com.project.visaruth.hangman;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddVolcabActivity extends AppCompatActivity {
    SQLiteDatabase mDb;
    MyDbHelper mHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_volcab);

        mHelper = new MyDbHelper(this);
        mDb = mHelper.getWritableDatabase();
    }

    public void addVolcab(View view){
        String sql = "INSERT INTO " + mHelper.TABLE_NAME + " (" + mHelper.COL_VOLCAB + ", " + mHelper.COL_MEANING
                + ", " + mHelper.COL_MODE + ") VALUES ('"+((TextView)findViewById(R.id.editText)).getText()+"', '"
                +((TextView)findViewById(R.id.editText2)).getText()
                +"', '"+((Spinner)findViewById(R.id.spinner)).getSelectedItem().toString()+"');";
        mDb.execSQL(sql);
        finish();
    }
}
