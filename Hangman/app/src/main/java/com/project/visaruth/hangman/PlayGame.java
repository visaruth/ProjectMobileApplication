package com.project.visaruth.hangman;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class PlayGame extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";
    SQLiteDatabase mDb;
    MyDbHelper mHelper;
    Cursor mCursor;
    ArrayList<String> volcabs;
    ArrayList<String> anwers;
    int countH;
    TextView volcab;
    String currentVolcab;
    TextView count;
    TextView mode;
    TextView meaning;
    int answersCount;
    Random ran;
    double ratio;
    StringBuilder forBuf;
    String bufCurVol;
    int hs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        hs = 0;

        meaning = (TextView)findViewById(R.id.textView6);

        mHelper = new MyDbHelper(this);
        mDb = mHelper.getWritableDatabase();
        mCursor = mDb.rawQuery("SELECT * FROM " + MyDbHelper.TABLE_NAME+ " WHERE "+MyDbHelper.COL_MODE+" = '"+getIntent().getStringExtra("mode")+"'  ORDER BY RANDOM();", null);

        mCursor.moveToFirst();

        volcab = (TextView)findViewById(R.id.textView);
        volcab.setText(mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_VOLCAB)));
        mode = (TextView) findViewById(R.id.textView3);
        mode.setText(getIntent().getStringExtra("mode"));

        if(mode.getText().equals("easy")) ratio = 0.3;
        else if(mode.getText().equals("normal")) ratio = 0.5;
        else ratio = 0.8;


        count = (TextView) findViewById(R.id.textView4);
        countH = 0;
        count.setText(countH+"/5");

        anwers = new ArrayList<String>();
        currentVolcab = mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_VOLCAB));
        bufCurVol = currentVolcab;

        meaning.setText(mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_MEANING)));

        answersCount = (int)Math.ceil(currentVolcab.length()*ratio);
        ran = new Random();
        forBuf = new StringBuilder(currentVolcab);
        for(int i=0;i<answersCount;i++){
            int forRan = ran.nextInt(currentVolcab.length());
            anwers.add(currentVolcab.charAt(forRan)+"");
            forBuf.setCharAt(forRan,'_');
        }
        currentVolcab = forBuf.toString();

        volcab.setText(currentVolcab);

    }

    public void clickChoice(View v){
        boolean pass = false,checkP = true;
        if(countH<5){
            countH++;
            String btnClick = v.getTag().toString();

            for(int i=anwers.size()-1;i>=0;i--){
                if(anwers.get(i).equalsIgnoreCase(btnClick)){
                    if(checkP){
                        countH--;
                        checkP=false;
                    }
                    for(int j=0;j<bufCurVol.length();j++){
                        if(bufCurVol.charAt(j)==anwers.get(i).charAt(0)){
                            forBuf = new StringBuilder(currentVolcab);
                            forBuf.setCharAt(j,btnClick.toLowerCase().charAt(0));
                            currentVolcab = forBuf.toString();
                            volcab.setText(currentVolcab);
                        }
                    }
                    anwers.remove(i);
                }
            }
            if(anwers.size()==0){
                pass=true;
            }

            count.setText(countH+"/5");
        }else{
            final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("ผิดหมดเลย");
            dialog.setIcon(android.R.drawable.ic_media_pause);
            dialog.setMessage("คุณตายแล้ววววววว");
            dialog.setPositiveButton("เริ่มเกมใหม่", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            dialog.show();
        }
        if(pass){
            hs++;
            if(mCursor.moveToNext()){
                currentVolcab = mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_VOLCAB));
                bufCurVol = currentVolcab;
                meaning.setText(mCursor.getString(mCursor.getColumnIndex(MyDbHelper.COL_MEANING)));
                countH = 0;
                count.setText(countH+"/5");
                answersCount = (int)Math.ceil(currentVolcab.length()*ratio);
                ran = new Random();
                forBuf = new StringBuilder(currentVolcab);
                for(int i=0;i<answersCount;i++){
                    int forRan = ran.nextInt(currentVolcab.length());
                    anwers.add(currentVolcab.charAt(forRan)+"");
                    forBuf.setCharAt(forRan,'_');
                }
                currentVolcab = forBuf.toString();
                volcab.setText(currentVolcab);
            }else{
                final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("จบเกม");
                dialog.setIcon(android.R.drawable.ic_media_pause);
                dialog.setMessage("คุณตอบถูกหมดเลย ต้องการเริ่มเกมใหม่มั้ย?");
                dialog.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                dialog.show();
                SharedPreferences shared_pref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                String bufHs = shared_pref.getString("hs", "No Pref");
                if(bufHs.equalsIgnoreCase("no pref")){
                    bufHs = "0";
                }
                if(Integer.parseInt(bufHs)<hs){
                    Toast.makeText(this,bufHs,Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = shared_pref.edit();
                    editor.putString("hs", hs+"");
                    editor.commit();
                }
            }
        }

    }

    public void pause(View v){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("หยุดเกม");
        dialog.setIcon(android.R.drawable.ic_media_pause);
        dialog.setMessage("คุณต้องการออกจากเกม?");
        dialog.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        dialog.setNegativeButton("ไม่", null);
        dialog.show();
    }

    public void onPause() {
        super.onPause();
        mHelper.close();
        mDb.close();
    }
}
