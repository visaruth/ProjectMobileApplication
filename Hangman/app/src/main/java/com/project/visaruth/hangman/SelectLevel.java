package com.project.visaruth.hangman;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SelectLevel extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_level);
    }

    public void play(View v){
        String level = v.getTag().toString();
        Intent intent = new Intent(SelectLevel.this,PlayGame.class);
        intent.putExtra("mode", level);
        startActivity(intent);
    }
}
