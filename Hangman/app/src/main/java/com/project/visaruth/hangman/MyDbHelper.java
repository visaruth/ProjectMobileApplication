package com.project.visaruth.hangman;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by megas on 11/30/2017.
 */

public class MyDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "BTS";
    private static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "Volcab";
    public static final String COL_VOLCAB = "volcabulary";
    public static final String COL_MEANING = "meaning";
    public static final String COL_MODE = "mode";

    public MyDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME +" (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_VOLCAB + " TEXT, " + COL_MEANING + " TEXT, "
                + COL_MODE + " TEXT);");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('dog', 'หมา', 'easy');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('cat', 'แมว', 'easy');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('bat', 'ค้างคาว', 'easy');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('coconut', 'มะพร้าว', 'normal');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('papaya', 'มะละกอ', 'normal');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('key', 'กุญแจ', 'easy');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('system', 'ระบบ', 'normal');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('clock', 'นาฬิกา', 'normal');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('hunter', 'นักล่า', 'normal');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('biology', 'ชีววิทยา', 'hard');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('math', 'เลข', 'hard');");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_NAME + " (" + COL_VOLCAB + ", " + COL_MEANING
                + ", " + COL_MODE + ") VALUES ('fan', 'พัดลม', 'easy');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
