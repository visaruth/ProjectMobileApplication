package com.project.visaruth.hangman;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class MainMenu extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_menu);

        SharedPreferences shared_pref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String hs = shared_pref.getString("hs", "No Pref");

        TextView hightScore = (TextView)findViewById(R.id.textView9);
        if(hs.equalsIgnoreCase("no pref")){
            hs = "0";
        }
        hightScore.setText("คะแนนสูงสุด : "+hs);
    }
    public void start(View v){
//        (new Toast(this)).makeText(this, "start!",
//                Toast.LENGTH_LONG).show();
        Intent intent = new Intent(MainMenu.this,SelectLevel.class);
        startActivity(intent);
    }
    public void edit(View v){
//        (new Toast(this)).makeText(this, "edit!",
//                Toast.LENGTH_LONG).show();
        Intent intent = new Intent(MainMenu.this,ListEditVocab.class);
        startActivity(intent);
    }
}
